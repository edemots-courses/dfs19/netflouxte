CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- DROP SCHEMA public;

CREATE SCHEMA public AUTHORIZATION postgres;

-- public.categories definition

-- Drop table

-- DROP TABLE public.categories;

CREATE TABLE public.categories (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	"name" varchar(255) NOT NULL,
	CONSTRAINT categories_pk PRIMARY KEY (id)
);


-- public.people definition

-- Drop table

-- DROP TABLE public.people;

CREATE TABLE public.people (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	"name" varchar(255) NOT NULL,
	"role" varchar(32) NOT NULL,
	CONSTRAINT people_pk PRIMARY KEY (id)
);


-- public.show_types definition

-- Drop table

-- DROP TABLE public.show_types;

CREATE TABLE public.show_types (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	"name" varchar(255) NOT NULL,
	CONSTRAINT show_types_pk PRIMARY KEY (id)
);


-- public.shows definition

-- Drop table

-- DROP TABLE public.shows;

CREATE TABLE public.shows (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	type_id uuid NOT NULL,
	title varchar(255) NOT NULL,
	country varchar(255) NOT NULL,
	release_year int4 NOT NULL,
	rating varchar(16) NOT NULL,
	duration varchar(16) NOT NULL,
	description text NOT NULL,
	CONSTRAINT titles_pk PRIMARY KEY (id),
	CONSTRAINT shows_fk FOREIGN KEY (type_id) REFERENCES show_types(id)
);


-- public.category_show definition

-- Drop table

-- DROP TABLE public.category_show;

CREATE TABLE public.category_show (
	category_id uuid NOT NULL,
	show_id uuid NOT NULL,
	CONSTRAINT category_show_fk FOREIGN KEY (show_id) REFERENCES shows(id),
	CONSTRAINT category_show_fk_1 FOREIGN KEY (category_id) REFERENCES categories(id)
);


-- public.people_show definition

-- Drop table

-- DROP TABLE public.people_show;

CREATE TABLE public.people_show (
	people_id uuid NOT NULL,
	show_id uuid NOT NULL,
	CONSTRAINT people_show_fk FOREIGN KEY (people_id) REFERENCES people(id) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT people_show_fk_1 FOREIGN KEY (show_id) REFERENCES shows(id) ON UPDATE CASCADE ON DELETE CASCADE
);