-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : mer. 24 mars 2021 à 11:43
-- Version du serveur :  10.4.18-MariaDB
-- Version de PHP : 8.0.3

START TRANSACTION;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `netflouxte`
--
CREATE DATABASE IF NOT EXISTS `netflouxte` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `netflouxte`;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT uuid(),
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `category_show`
--

CREATE TABLE `category_show` (
  `category_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `show_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `people`
--

CREATE TABLE `people` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT uuid(),
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(32) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `people_show`
--

CREATE TABLE `people_show` (
  `people_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `show_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `shows`
--

CREATE TABLE `shows` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT uuid(),
  `type_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `release_year` int(4) NOT NULL,
  `rating` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `duration` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `show_types`
--

CREATE TABLE `show_types` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT uuid(),
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `category_show`
--
ALTER TABLE `category_show`
  ADD KEY `category_show_fk` (`show_id`),
  ADD KEY `category_show_fk_1` (`category_id`);

--
-- Index pour la table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `people_show`
--
ALTER TABLE `people_show`
  ADD KEY `people_show_fk` (`people_id`),
  ADD KEY `people_show_fk_1` (`show_id`);

--
-- Index pour la table `shows`
--
ALTER TABLE `shows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shows_fk` (`type_id`);

--
-- Index pour la table `show_types`
--
ALTER TABLE `show_types`
  ADD PRIMARY KEY (`id`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `category_show`
--
ALTER TABLE `category_show`
  ADD CONSTRAINT `category_show_fk` FOREIGN KEY (`show_id`) REFERENCES `shows` (`id`),
  ADD CONSTRAINT `category_show_fk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Contraintes pour la table `people_show`
--
ALTER TABLE `people_show`
  ADD CONSTRAINT `people_show_fk` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `people_show_fk_1` FOREIGN KEY (`show_id`) REFERENCES `shows` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `shows`
--
ALTER TABLE `shows`
  ADD CONSTRAINT `shows_fk` FOREIGN KEY (`type_id`) REFERENCES `show_types` (`id`);
SET FOREIGN_KEY_CHECKS=1;

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
