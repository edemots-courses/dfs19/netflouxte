<?php

require('autoload.php');
require('routes.php');

use Core\Routing\Router;

// On débute notre application en créant le routeur
$router = new Router();