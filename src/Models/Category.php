<?php

namespace Models;

use Core\Database\Model;

class Category extends Model
{
    protected $table = 'categories';
}