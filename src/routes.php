<?php

use Core\Routing\Route;

Route::get('/', 'HomepageController@index');
Route::get('/shows', 'ShowsController@index');
Route::get('/shows/(id)', 'ShowsController@show');