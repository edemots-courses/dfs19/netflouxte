<?php

/**
 * Permet d'enregistrer une fonction pour charger automatiquement
 * Les classes lorsqu'ells sont instantiées
 */
spl_autoload_register(function ($classname) {
    // On créé le chemin absolu en changeant les "\" par des "/" et on ajout ".php" à la fin
    $file = __DIR__.'/'.str_replace('\\', DIRECTORY_SEPARATOR, $classname).'.php';

    if (file_exists($file)) {
        // Si le fichier éxiste, on le require
        require($file);
    }
});