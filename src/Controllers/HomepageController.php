<?php

namespace Controllers;

use Core\View;

class HomepageController
{
    public function index()
    {
        new View('homepage');
    }
}