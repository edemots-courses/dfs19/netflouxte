<?php

namespace Controllers;

use Core\View;
use Models\Show;

class ShowsController
{
    /**
     * Affiche l'ensemble des Shows de la BDD
     */
    public function index()
    {
        $shows = Show::select(['id', 'title', 'release_year'])->get();

        new View('shows', compact("shows"));
    }

    /**
     * Affiche seulement les détails du Show qui porte l'id passé en paramètre
     */
    public function show($id)
    {
        $show = Show::select(['title', 'release_year', 'description'])
                ->where('id', '=', $id)
                ->get()[0];

        new View('show', compact("show"));
    }
}