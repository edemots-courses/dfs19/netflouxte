<?php

namespace Core;

use Exception;

class View
{
    private $template;

    private $data;

    public function __construct(string $template, array $data = [])
    {
        $template = __DIR__.'/../Views/'.$template.'.php';
        // On vérifie que le template éxiste bien dans le dossier Views
        if (!file_exists($template)) {
            throw new Exception("La vue $template n'éxiste pas.");
        }

        $this->template = $template;
        $this->data = $data;
    }

    /**
     * Est appelée dès qu'on ne fait plus référence à l'objet Router ou dans
     * n'importe quel ordre pendant la séquence d'arrêt de PHP.
     */
    public function __destruct()
    {
        // On créé une variable par élément du tableau $this->data
        // Chaque variable créée porte le nom de la clé, et possède en valeur
        // celle qui était insérée dans le tableau
        extract($this->data);
        // On require notre template pour l'afficher
        require $this->template;
    }
}