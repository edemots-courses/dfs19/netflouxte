<?php

namespace Core\Database;

class Model
{
    /** Donne la possiblité aux enfants de définir le nom de leur table */
    protected $table;

    /**
     * Fonction permettant de récupérer le nom de la table
     * en fonction d'un attribut ou de le deviner
     */
    public function getTable(): string
    {
        if (isset($this->table)) {
            return $this->table;
        }

        return $this->guessTableName();
    }

    /**
     * On devine le nom de la table en fonction du nom
     * du Model (Show = shows, ShowType = show_types, ...)
     */
    private function guessTableName(): string
    {
        // On récupère le nom du Model utilisé et on l'explose
        // dans un tableau en fonction du caractère "\"
        // ['Models', 'ShowType']
        // get_called_class() = static::class
        $callingClass = explode('\\', get_called_class());
        // On sort le dernier élément de notre tableau qui
        // correspond au nom du Model
        // ShowType
        $classname = array_pop($callingClass);

        // Si le nom extrait est ecrit en CamelCase,
        // on le transform en snake_case
        $classname = preg_replace(
            // Si une majuscule est précédée d'une minuscule on ajoute un "_" avant
            '/(?<=[a-z])(?=[A-Z])/',
            "_",
            $classname
        );

        // On met tout au minuscule et on ajoute un "s" à la fin
        return strtolower($classname).'s';
    }

    /**
     * Transfère l'appel d'une méthode du query builder (select, where, get, ...)
     * sur une instance de QueryBuilder.
     * Elle est appelée lorsqu'on appelle, sur une classe, une méthode de manière statique qui n'est pas définie
     * ou inaccessible.
     */
    public static function __callStatic($name, $arguments)
    {
        // static porte le nom de la classe appelante
        $queryBuilder = new QueryBuilder(new static());

        return $queryBuilder->$name(...$arguments);
    }
}