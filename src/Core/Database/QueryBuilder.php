<?php

namespace Core\Database;

use PDO;

// SELECT title, realease_date, ...
// FROM table
// WHERE
// AND
// AND
// ORDER BY column ASC, release_date DESC
// LIMIT
// OFFSET
/**
 * Permet de construire une requête pas à pas
 */
class QueryBuilder
{
    private $model;

    private $columns = ['*'];
    private $whereConditions = [];
    private $orderBy;
    private $limit;
    private $offset;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Methode pour définir les colonnes à récupérer dans le "SELECT"
     */
    public function select(array $columns)
    {
        $this->columns = $columns;

        return $this;
    }

    /**
     * Méthode pour construire un ensemble de conditions "WHERE"
     */
    public function where(string $column, string $operator, mixed $value)
    {
        if (gettype($value) === 'array') {
            // MySQL
            // $value = '(\''.implode('\', \'', $value).'\')';
            // PostgreSQL
            $value = 'ANY {\''.implode('\', \'', $value).'\'}';
        } else {
            $value = "'".$value."'";
        }

        $this->whereConditions[] = "$column $operator $value";

        return $this;
    }

    /**
     * Méthode pour définir un ensemble de colonnes sur lesquelles on souhaite
     * ordonner nos résultats
     * @param ['colonne' => 'sens', ...] $columns
     */
    public function orderBy(array $columns)
    {
        $orderBy = 'ORDER BY ';
        // On parcourt les différentes valeurs pour construire
        // notre ORDER BY
        foreach ($columns as $name => $direction) {
            // $orderBy = $orderBy."$name $direction, ";
            $orderBy .= "$name $direction, ";
        }
        // role ASC, release_date DESC,
        $orderBy = trim($orderBy, ', '); // role ASC, release_date DESC

        $this->orderBy = $orderBy.' ';

        return $this;
    }

    /**
     * Méthode pour définir une limite de résultats à récupérer
     */
    public function limit(int $limit)
    {
        $this->limit = 'LIMIT '.$limit.' ';

        return $this;
    }

    /**
     * Méthode pour définir un décalage sur la requête
     * Si offset = 5, alors on sautera les 5 permiers résultats
     * et on commencera à récupérer à partir du 6ème
     */
    public function offset(int $offset)
    {
        $this->offset = 'OFFSET '.$offset.' ';

        return $this;
    }

    /**
     * Permet d'indiquer qu'on termine notre requête
     * Cette méthode va construire la requête SQL et
     * l'envoyer en BDD pour nous retourner les résultats
     */
    public function get()
    {
        $sql = '';
        // Si nous n'avons pas utilisé la méthode "select()"
        $sql = 'SELECT '.implode(', ', $this->columns).' ';
        // On récupère le nom de la table grâce au model
        $sql .= 'FROM '.$this->model->getTable().' ';

        if (!empty($this->whereConditions)) {
            $sql .= 'WHERE '.implode(' AND ', $this->whereConditions).' ';
        }

        // On ajoute un orderBy si il éxiste
        $sql .= $this->orderBy;
        // On ajoute un limit si il éxiste
        $sql .= $this->limit;
        // On ajoute un offset si il éxiste
        $sql .= $this->offset;

        // On prépare avec PDO la requête SQL
        $request = DB::prepare($sql);
        // On éxecute la requête en BDD
        $request->execute();

        // Grâce aux paramètres PDO::FETCH_CLASS et get_class($this->model)
        // On dit à PDO de nous renvoyer des instances de la classe passée en
        // second paramètre, qui ont pour attribut les données récupérées en BDD
        return $request->fetchAll(PDO::FETCH_CLASS, get_class($this->model));
    }
}