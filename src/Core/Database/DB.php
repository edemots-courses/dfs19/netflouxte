<?php

namespace Core\Database;

use PDO;

/**
 * Singleton
 */
class DB
{
    /** @var ?PDO L'unique instance de PDO de notre application */
    private static $instance = null;

    /**
     * Créer l'instance si elle n'éxiste pas et la retourne
     */
    private static function getInstance(): PDO
    {
        // On vérifie si l'instance éxiste
        if (self::$instance === null) {
            // On se connecte à notre BDD via PDO
            // Pour MySQL: mysql:host=localhost;dbname=database_name, 'username', 'password'
            self::$instance = new PDO('pgsql:host=postgres;dbname=postgres', 'postgres', 'secret');
        }

        // On retourne l'instance de PDO
        return self::$instance;
    }

    /**
     * Appelée lorsque je fais un appel statique sur un méthode de ma classe qui n'éxiste pas ou qui n'est pas accessible
     * @param string $name nom de la fonction qu'on essaye d'appeler
     * @param array $arguments les arguments passés à la fonction qu'on appelle
     */
    public static function __callStatic($name, $arguments)
    {
        // (...) = Spread operator
        // self::getInstance()->query('SELECT * FROM shows LIMIT 5;') par exemple
        return self::getInstance()->$name(...$arguments);
    }
}