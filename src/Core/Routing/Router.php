<?php

namespace Core\Routing;

class Router
{
    const SUPPORTED_METHOD = ['GET', 'POST'];

    private static $routes = [];

    private $request;

    public function __construct()
    {
        // On construit la requête au même moment que le Router
        $this->request = new Request();
    }

    public static function addRoute(string $method, string $url, string $dispatcher)
    {
        $url = rtrim($url, '/');
        // en JS: { $url, $dispatcher } => { url: $url, dispatcher: $dispatcher }
        // $this->routes[$method][] = ['url' => $url, 'dispatcher' => $dispatcher];
        self::$routes[$method][] = compact("url", "dispatcher");
        // equivalent de array_push($this->routes[$method], compact("url", "dispatcher"));

        /**
         * [
         *      "GET" => [
         *          0 => [
         *              'url' => $url,
         *              'dispatcher' => $dispatcher
         *          ], ...
         *      ],
         *      "POST" => [...]
         * ]
         */
    }

    /**
     * Permet de lancer l'éxécution du Router
     */
    private function run(): void
    {
        // On vérifie en premier lieu que la méthode utilisée par l'utilisateur
        // éxiste dans notre tableau de routes
        if (array_key_exists($this->request->getMethod(), self::$routes)) {
            // Si c'est le cas, vérifie toutes les routes de cette méthode
            // pour trouver celle qui correspond à l'uri de l'utilisateur
            $this->handleRoute(self::$routes[$this->request->getMethod()]);

            return;
        }

        // Sinon on envoie une 404
        header($_SERVER['SERVER_PROTOCOL']." 404 Not Found");
    }

    /**
     * Vérifie si la route de l'utilisateur correspond à une route
     * déclarée dans le fichier routes.php
     */
    private function handleRoute(array $routes)
    {
        // On récupère l'uri de l'utilisateur depuis la requête
        $uri = $this->request->getUri();

        foreach ($routes as $route) {
            // Si notre route possède un paramètre entre "()", ex: (id)
            // On modifie la syntaxe de manière à prendre en compte
            // n'importe quelle valeur en paramètre
            // ex: '/(id)' devient '/(.*?)'
            $route['url'] = preg_replace('#/\((.*?)\)#', '/(.*?)', $route['url']);

            // On construit notre regex à partir de ce qu'on a fait précèdemment
            $regex = '#^'.$route['url'].'$#';
            // On vérifie que la regex et l'uri de l'utilisateur correspondent
            if (preg_match_all($regex, $uri, $matches, PREG_SET_ORDER)) {
                // On laisse de côté le premier élément de l'array: le full match
                // pour ne récupèrer que ce qui se trouvait dans le groupe capturant,
                // c'est-à-dire ce qui était entre parenthèses
                $params = array_slice($matches[0], 1);

                // On appelle traite le dispatcher de manière à appeler une méthode
                // d'un controller avec les paramètres reçus.
                $this->invoke($route['dispatcher'], $params);
            }
        }
    }

    /**
     * Traite une chaîne de caractères de la forme "NomDuController@nomDeLaMethode"
     * pour ensuite appeler la bonne méthode sur le bon controller
     */
    private function invoke(string $dispatcher, array $params)
    {
        // $dispatcher = explode('@', $dispatcher);
        // $controller = $dispatcher[0];
        // $action = $dispatcher[1];
        // est la même chose que :
        [$controller, $action] = explode('@', $dispatcher);
        // On ajoute le namespace devant le nom de notre Controller
        $controller = 'Controllers\\'.$controller;

        // On vérifie que la class du controller éxiste et qu'elle possède bien
        // la méthode qu'on veut appeler dessus
        if (class_exists($controller) && method_exists($controller, $action)) {
            // On instancie notre controller
            $controller = new $controller();
            // On appelle l'action sur notre instance avec les bons paramètres
            $controller->$action(...$params);
        }
    }

    /**
     * Est appelée dès qu'on ne fait plus référence à l'objet Router ou dans
     * n'importe quel ordre pendant la séquence d'arrêt de PHP.
     */
    public function __destruct()
    {
        // On lance l'éxécution de notre Router
        $this->run();
    }
}