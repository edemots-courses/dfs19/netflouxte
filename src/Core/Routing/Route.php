<?php

namespace Core\Routing;

abstract class Route
{
    public static function __callStatic($name, $arguments)
    {
        // On met la méthode en MAJUSCULE, ex: get => GET
        $method = strtoupper($name);
        // Si elle est supportée par notre Routeur on l'ajoute
        if (in_array($method, Router::SUPPORTED_METHOD)) {
            Router::addRoute($method, $arguments[0], $arguments[1]);
        }
    }
}