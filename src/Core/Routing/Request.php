<?php

namespace Core\Routing;

class Request
{
    private $uri;
    private $method;
    private $body = [];

    public function __construct()
    {
        $this->uri = rtrim($_SERVER['REQUEST_URI'], '/');
        $this->method = $_SERVER['REQUEST_METHOD'];
        // $this->body = [];
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getBody(): array
    {
        return $this->body;
    }
}